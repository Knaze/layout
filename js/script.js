$(document).ready(function(){
    /**
     * При прокрутке страницы, показываем или срываем кнопку
     */
    $(window).scroll(function () {
        // Если отступ сверху больше 50px то показываем кнопку "Наверх"
        if ($(this).scrollTop() > 50) {
            $('#button-up').fadeIn();
            $('#navbar').addClass('sticky-top');
        } else {
            $('#button-up').fadeOut();
            $('#navbar').removeClass('sticky-top');
        }
    });

    /** При нажатии на кнопку мы перемещаемся к началу страницы */
    $('#button-up').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 500);
        return false;
    });

});

var container = $('.container'),
    menu = $('.menu'),
    ws, as, abh, agh;
function position(){
    ws = $(window).scrollTop(); // отступ верхней границы экрана от страницы
    as = container.offset().top; // отступ контейнера, содержащего меню от страницы
    abh = container.height();
    agh = menu.height();
    if (ws > (as+(abh-agh))) {
        // ничего не делаем, чтобы меню прилипло к низу контейнера, когда прокрутим ниже чем контейнер
    }
    else if (ws > as) {
        menu.css('top', (ws-as+50)+'px'); // тут мы в пределах контейнера, расчитываем отступ чтобы меню прилипло к верху экрана
    }
    else {
        menu.css('top', '0px'); // если мы выше контейнера с меню - прилепить меню к верху контейнера
    }
}

position();

$(document).on('scroll', function(){
    position();
});

